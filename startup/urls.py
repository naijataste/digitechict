from django.conf.urls import url
from . import views
from .views import emailView, successView


urlpatterns = [
	url(r'^$', views.index, name='index'),

	# About Pages
	url(r'^about/faqs/$', views.faqs, name='faqs'),
	url(r'^about/contact-us/$', emailView, name='contact'),
	url(r'success/', successView, name='success'),
	url(r'^about/$', views.about, name='who-we-are'),

	# Blog
	url(r'^security-tips/$', views.blog, name='blog'),
	url(r'^content/(?P<post>[-\w]+)/$', views.post_detail_view, name='post_detail_view'),

	# Shop
	url(r'^products/$', views.products, name='shop'),
]
