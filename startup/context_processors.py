from .models import *


def config(request):
    configs = SiteConfig.objects.all()

    return{
        'configs': configs
    }


def blog(request):
    blogs = BlogPost.objects.all()

    return{
        'blogs': blogs
    }

