from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class SiteConfig(models.Model):
    site_name = models.CharField(max_length=50, blank=False)
    site_title = models.CharField(max_length=50, blank=False)
    site_mail = models.CharField(max_length=50, blank=False)
    site_mobile = models.CharField(max_length=50, blank=False)
    site_fb = models.CharField(max_length=50, blank=False)
    site_tw = models.CharField(max_length=50, blank=False)
    site_ig = models.CharField(max_length=50, blank=False)
    site_yt = models.CharField(max_length=50, blank=False)
    site_lk = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return self.site_name


class BlogPost(models.Model):
    title = models.CharField(max_length=100, blank=True, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    content = models.TextField()
    img = models.ImageField(upload_to='blog', default='/static/img/post.jpg')

    def __str__(self):
        return self.title


class Product(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField()
    price = models.IntegerField(max_length=50)
    image = models.ImageField(upload_to='products', default='/static/img/products.jpg')

    def __str__(self):
        return self.name
