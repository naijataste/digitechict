from django.shortcuts import render
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import ContactForm


def emailView(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
        	name = form.cleaned_data['name']
        	subject = form.cleaned_data['subject']
        	from_email = form.cleaned_data['from_email']
        	message = form.cleaned_data['message']
        	try:
        		send_mail(name, subject, message, from_email, ['naijataste@gmail.com'])
        	except BadHeaderError:
        		return HttpResponse('Invalid header found.')
        	return redirect('success')
    return render(request, "about/contact.html", {'form': form})

def successView(request):
    return HttpResponse('Success! Thank you for your message.')


def index(request):
	return render(request, 'index.html')


def faqs(request):
	return render(request, 'about/faqs.html')


def about(request):
	return render(request, 'about/who-we-are.html')


def post_detail_view(request, post):
    post = get_object_or_404(Post, slug=post)
    return render(request, 'blog/single.html', {'post': post})


def get_absolute_url(self):
	return reverse('blog:post_detail_view', args=[self.slug])


def blog(request):
	return render(request, 'blog/index.html')


def products(request):
	return render(request, 'shop/index.html')


def contact(request):
	return render(request, 'about/contact.html')
