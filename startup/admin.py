from django.contrib import admin
from .models import *


admin.site.register(SiteConfig)
admin.site.register(BlogPost)
